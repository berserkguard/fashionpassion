from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.base import runTouchApp
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen
from Util import TitleBar
from sets import Set
from IconButton import IconButton
from kivy.graphics import *
import os

class WardrobeScreen(Screen):

    def __init__(self, appData, **kwargs):
        super(WardrobeScreen, self).__init__(**kwargs)

        self.name = "WardrobeScreen"
        self.appData = appData
        self.checkedClothing = []

        self.layout = layout = FloatLayout()
        layout.add_widget(self.createTop())
        layout.add_widget(self.createMiddle())
        layout.add_widget(self.createBottom())
        self.add_widget(layout)

    def on_enter(self):
        if not self.middle:
            return

        self.middle.clear_widgets()
        self.layout.add_widget(self.createMiddle())

    def deleteConfirmation(self, obj):
        if len(self.checkedClothing) == 0:
            grid = GridLayout(rows=2, padding=10, spacing=20)
            grid.add_widget(Label(text='Please select items to delete'))
            but = Button(text='Close')
            grid.add_widget(but)
            popup = Popup(title='Error', content=grid, size_hint=(.4,.3))
            but.bind(on_press=popup.dismiss)
            popup.open()
        else:
            content = BoxLayout(orientation='horizontal', spacing=0, padding=0, height=100, size_hint=(1,None))
            closePopup = Button(text='No', size_hint=(.4,.4))
            yes = Button(text='Yes', size_hint=(.4,.4))
            content.add_widget(yes)
            content.add_widget(closePopup)
            popup = self.popup = Popup(title='Are you sure you want to delete?', content=content, size_hint=(.2, .2))
            closePopup.bind(on_press=popup.dismiss)
            yes.bind(on_press=self.deleteItems)
            self.popup.open()

    def modifyConfirmation(self, obj):
        if len(self.checkedClothing) == 0:
            grid = GridLayout(rows=2, padding=10, spacing=20)
            grid.add_widget(Label(text='Please select items to modify'))
            but = Button(text='Close')
            grid.add_widget(but)
            popup = Popup(title='Error', content=grid, size_hint=(.4,.3))
            but.bind(on_press=popup.dismiss)
            popup.open()
        else:
            self.middle.clear_widgets()
            self.appData.modifyClothingTags = Set()
            for i, elem in enumerate(self.checkedClothing):
                for j, elem2 in enumerate(self.appData.clothing):
                    if elem2[0][0] == elem:
                        self.appData.modifyClothingTags = self.appData.modifyClothingTags.union(elem2[0][1])
                        self.appData.modifyClothing.add(elem2[0][0])
            self.checkedClothing = []
            self.appData.sm.transition.direction = 'left'
            self.appData.sm.current = "ModifyTagScreen"

    def deleteItems(self, obj):
        self.popup.dismiss()
        for i, elem in enumerate(self.checkedClothing):
            for j, elem2 in enumerate(self.appData.clothing):
                if elem2[0][0] == elem:
                    self.appData.clothing.pop(j)
        self.checkedClothing = []
        self.on_enter()

    def createTagsDropdown(self):
        tagsDropdown = self.tagsDropdown = DropDown(size_hint=(None, 0.1), dismiss_on_select=True, max_height=200, auto_width=False, minimum_width=50)
        
        # Add a button for each tag
        """
        for tag in self.appData.globalTagList:
            btn = ToggleButton(text=tag, size_hint_y=None, height=40)
            tagsDropdown.add_widget(btn)
        """
                
        tagsButton = Button(text='Tags', pos_hint={'x':.7, 'y':.7}, size_hint=(0.1, 0.1))
        tagsButton.bind(on_release=tagsDropdown.open)
        
        return tagsButton
    
    def createTop(self):
        return TitleBar("Wardrobe", self.appData, "HomeScreen")

    def createMiddle(self):
        middle = self.middle = GridLayout(rows=3, padding=10, spacing=20, size_hint=(None,1))
        middle.bind(minimum_width=middle.setter('width'))
        for i, elem in enumerate(self.appData.clothing):
            btn = IconButton(text='cloth %d' % i, width=243, size_hint=(None, 1), source=elem[0][0])
            btn.bind(on_press=self.updateGraphics)
            middle.add_widget(btn)
        root = ScrollView(size_hint=(1, .6), pos_hint={'center_x': .5, 'center_y': .4}, do_scroll_x=True)
        root.add_widget(middle)
        return root

    def createBottom(self):
        bot = FloatLayout()
        x_base = 0.02 #x position base
        y_base = 0.02 #y position base
        bottom_size = (.1,.05) #bottom button size
        bottom_increment = 0.287 #x position increment size

        add = Button(text='add', size_hint=bottom_size, pos_hint={'x':x_base, 'y':y_base})
        add.bind(on_press=self.addItemScreen)
        bot.add_widget(add)

        delete = Button(text='delete', size_hint=bottom_size, pos_hint={'x':x_base+bottom_increment, 'y':y_base})
        delete.bind(on_press=self.deleteConfirmation)
        bot.add_widget(delete)

        modify = Button(text='modify', size_hint=bottom_size, pos_hint={'x':x_base+2*bottom_increment, 'y':y_base})
        modify.bind(on_press=self.modifyConfirmation)
        bot.add_widget(modify)

        bot.add_widget(Button(text='dirty', size_hint=bottom_size, pos_hint={'x':x_base+3*bottom_increment, 'y':y_base}))
        return bot

    def addItemScreen(self, obj):
        self.appData.sm.transition.direction = 'left'
        self.appData.sm.current = "AddItemScreen"

    def updateGraphics(self, obj):
        obj.canvas.clear()
        if obj.checked == False:
            with obj.canvas:
                Color(.5, .5, .5)
                Rectangle(pos=obj.pos, size=obj.size)
                Color(1, 1, 1)
                cx = obj.center_x - obj.texture_size[0] / 2.
                cy = obj.center_y - obj.texture_size[1] / 2.
                Rectangle(texture=obj.texture, pos=(cx, cy), size=obj.texture_size)
            self.checkedClothing.append(obj.source)
            obj.checked = True
        else:
            with obj.canvas:
                Color(0, 0, 0)
                Rectangle(pos=obj.pos, size=obj.size)
                Color(1, 1, 1)
                cx = obj.center_x - obj.texture_size[0] / 2.
                cy = obj.center_y - obj.texture_size[1] / 2.
                Rectangle(texture=obj.texture, pos=(cx, cy), size=obj.texture_size)
            self.checkedClothing.remove(obj.source)
            obj.checked = False
