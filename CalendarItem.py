from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
from kivy.uix.widget import Widget
from IconButton import IconButton
from kivy.uix.image import Image
from kivy.graphics import *
from Outfits import Outfits
from AppData import AppData

class PlanOutfitButton(Widget):
    
    def __init__(self, appData, day, idx, **kwargs):
        super(PlanOutfitButton, self).__init__(**kwargs)

        self.sm = appData.sm;
        self.appData = appData;
        self.day = day;
        self.idx = idx;

        self.bind(pos=self.redraw, size=self.redraw)

    def redraw(self, wid, args):
        outfits = Outfits(self);
        self.canvas.clear();
        self.canvas.add(outfits.mannequin);
        
        for clothing in self.appData.userOutfits[self.idx]:
            self.canvas.add(outfits.getFull(clothing.name));
        
        
    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            self.appData.planOutfitDay = self.day
            if self.idx == 0:
                self.appData.planOutfitDay = "Today"
            self.appData.planOutfitIdx = self.idx
            self.sm.transition.direction = "left"
            self.sm.current = "PlanOutfitScreen"

            return True

class CalendarItem(BoxLayout):

    def __init__(self, appData, day, idx, lo, hi, weather):
        self.sm = appData.sm;
        self.day = day;
        self.lo = lo;
        self.hi = hi;
        self.weather = weather;
        self.appData = appData;
        self.idx = idx;
        
        #self.layout = layout = BoxLayout(orientation='vertical', spacing=0, padding=0, width=250, size_hint=(None,1));
        super(CalendarItem, self).__init__(orientation='vertical', spacing=0, padding=0, width=250, size_hint=(None,1));
        
        self.add_widget(self.createTop());
        self.add_widget(self.createMiddle());
        self.add_widget(self.createBottom());
    
    def refresh(self):
        if self.btn:
            self.btn.redraw(None, None);
    
    def createTop(self):
        return Label(text=self.day, font_size='20sp', size_hint=(1, 0.1))
        
    def createMiddle(self):
        self.btn = PlanOutfitButton(self.appData, self.day, self.idx, size_hint=(1, 0.5))
        
        return self.btn
    
    def createBottom(self):
        bottom = GridLayout(cols=1, padding=5, spacing=0, size_hint=(1, 0.4))
        bottom.add_widget(Image(source='assets//' + str(self.weather) + 'icon.png', size_hint=(1, 0.5)))
        bottom.add_widget(Label(text='Hi: ' + str(self.hi), size_hint=(1, 0.25)))
        bottom.add_widget(Label(text='Lo: ' + str(self.lo), size_hint=(1, 0.25)))
        return bottom
