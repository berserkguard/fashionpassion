from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from IconButton import IconButton
from kivy.graphics import *

class TitleBar(GridLayout):
    def __init__(self, curScreen, appData, prevScreen, **kwargs):
        super(TitleBar, self).__init__(rows=1, padding=5, spacing=0, size_hint=(None, 0.1), pos_hint={'x':0, 'y':.9});
        
        self.curScreen = curScreen
        self.appData = appData
        self.prevScreen = prevScreen
        
        btn = IconButton(on_press=self.button_pushed, source='assets//backward.png');
        btn.width = btn.height;
        self.add_widget(btn);
        
        lbl = Label(text="                " + self.curScreen, font_size='30sp');
        lbl.texture_update()
        self.add_widget(lbl);
    
    def button_pushed(self, obj):
        self.appData.modifyClothing.clear()
        self.appData.modifyClothingTags.clear()
        self.appData.sm.transition.direction = 'right'
        self.appData.sm.current = self.prevScreen


class ConfirmationPopup(Popup):
    def __init__(self, callback, **kwargs):
        content = BoxLayout(orientation='horizontal', spacing=0, padding=0, height=100, size_hint=(1,None))
        
        self.callback = callback;
        
        yes = Button(text='Yes', size_hint=(.4,.4))
        content.add_widget(yes)
        
        no = Button(text='No', size_hint=(.4,.4))
        content.add_widget(no)
        
        no.bind(on_press=self.dismiss)
        yes.bind(on_press=self.onYes)
        
        super(ConfirmationPopup, self).__init__(title='Are you sure you want to delete?', content=content, size_hint=(.2, .2))

    def onYes(self, instance):
        self.dismiss();
        self.callback();
