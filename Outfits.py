from kivy.graphics import *

class Outfits():
    def __init__(self, root):
        ratio = 166.0 / 384.0;

        # calculate size
        scaleX = root.width / 166.0;
        scaleY = root.height / 384.0;
        
        self.idealSize = idealSize = [None, None];
        if scaleX > scaleY:
            # Height is limiting factor
            idealSize[1] = root.height;
            idealSize[0] = root.height * ratio;
        else:
            # Width is limiting factor
            idealSize[0] = root.width;
            idealSize[1] = root.width / ratio;
        
        excessX = 0;
        
        # calculate position
        self.idealPos = idealPos = [root.x, root.y];
        excessX = root.width - idealSize[0];
        idealPos[0] += excessX / 2.0;

        # Now for icons
        self.iconSize = min(root.width, root.height);
        
        self.idealPosIcon = idealPosIcon = [root.x, root.y];
        excessIconX = root.width - self.iconSize;
        idealPosIcon[0] += excessIconX / 2.0;

        # Mannequin
        self.mannequin = InstructionGroup()
        self.mannequin.add(Rectangle(source="assets//mannequin_blank.png", pos=idealPos, size=idealSize))
    
    def getFull(self, item):
        inst = InstructionGroup();
        inst.add(Rectangle(source="assets//mannequin_clothes//" + item + ".png", pos=self.idealPos, size=self.idealSize));
        return inst

    def getIcon(self, item):
        inst = InstructionGroup();
        inst.add(Rectangle(source="assets//clothing_icons//" + item + ".png", pos=self.idealPosIcon, size=(self.iconSize, self.iconSize)));
        return inst