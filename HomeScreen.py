from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.event import EventDispatcher
from kivy.uix.behaviors import ButtonBehavior
from IconButton import IconButton
import random
#currently, the buttons are defined as IconButtons, 
#which are created in a separate file in case anyone else wants to use them.
#also, clicking and releasing buttons currently just causes it to print the file location
#of the image the button uses, because I don't know how to implement the screen transition

class HomeScreen(Screen):
    def __init__(self, appData, **kwargs):
        super(HomeScreen, self).__init__(**kwargs)
        self.name = "HomeScreen"

        self.sm = appData.sm
        self.appData = appData
        layout = BoxLayout()
  
        layout.orientation = 'vertical'
                
        layout.add_widget(self.createTop())
        layout.add_widget(self.createMiddle())
        layout.add_widget(self.createBottom())

        self.add_widget(layout)
        
    def createTop(self):
        top = BoxLayout(orientation='horizontal')
        logo = Image(source='assets//logo.png')
        top.add_widget(logo)
        return top
    
    def createMiddle(self):
        middle = BoxLayout(orientation='horizontal')  
        weatherBox = BoxLayout(orientation='vertical')
        
        todayLabel = Label(text='Today:', size_hint=(1, 0.2))
        
        if(random.randint(0, 10)%2 == 0):
            weather = Image(source='assets//cloudy weather.png', size_hint=(1, 1))
            tempLabel = Label(text='Low: 50F, High: 65F', size_hint=(1, 0.2))
        else:
            weather = Image(source='assets//sunny weather.png', size_hint=(1, 1))
            tempLabel = Label(text='Low: 86F, High: 90F', size_hint=(1, 0.2))

        weatherBox.add_widget(todayLabel)
        weatherBox.add_widget(weather)
        weatherBox.add_widget(tempLabel)
            
        planButton = IconButton(on_press=self.button_pushed, source='assets//planoutfit.png')
                 
        middle.add_widget(weatherBox)
        middle.add_widget(planButton)
        return middle
    
    def createBottom(self):
        bottom = BoxLayout(orientation='horizontal')     
                
        wardrobeButton = IconButton(on_press=self.button_pushed, source='assets//hanger.png')
        calendarButton = IconButton(on_press=self.button_pushed, source='assets//calendar.png')       
                
        bottom.add_widget(wardrobeButton)
        bottom.add_widget(calendarButton)
        
        return bottom
    
    def button_pushed(self, obj):
        if obj.source == "assets//calendar.png":
            self.sm.transition.direction = 'left'
            self.sm.current = "CalendarScreen"
        elif obj.source == "assets//planoutfit.png":
            self.appData.planOutfitDay = "Today"
            self.appData.planOutfitIdx = 0;
            self.sm.transition.direction = 'left'
            self.sm.current = "PlanOutfitScreen"
        elif obj.source == "assets//hanger.png":
            self.sm.transition.direction = 'left'
            self.sm.current = "WardrobeScreen"
        
        print "button pushed: " + obj.source
