from kivy.uix.screenmanager import ScreenManager, Screen
from sets import Set
from Clothing import Clothing
import os
import pickle

class AppData():
    def __init__(self):
        self.sm = ScreenManager();
        
        self.globalTagList = Set()
        self.globalTagList.add("tops")
        self.globalTagList.add("bottoms")
        self.globalTagList.add("accessories")
        self.globalTagList.add("footwear")
        
        self.planOutfitDay = "Today"
        self.planOutfitIdx = 0
        self.clothing = []
        self.modifyClothingTags = Set()
        self.modifyClothing = Set()
        
        """
        # Hard-coded clothing for testing
        boots = Clothing("boots");
        jeans = Clothing("jeans");
        sandals = Clothing("sandals");
        tshirt = Clothing("tshirt");
        shorts = Clothing("shorts");
        skirt = Clothing("skirt");
        tanktop = Clothing("tanktop");
        
        # Hard-coded outfits for testing
        outfit1 = Set([boots, tshirt, jeans]);
        outfit2 = Set([sandals, skirt, tanktop]);
        outfit3 = Set([tshirt, shorts, boots]);
        """
        self.clothingType = None
        
        # Allocate space for 7 outfits ahead of time
        self.userOutfits = [Set(), Set(), Set(), Set(), Set(), Set(), Set()]
        
        # Data that needs to be saved to & from the filesystem (add items as needed)
        self.saveData = [self.globalTagList, self.userOutfits, self.clothing]
        
    def writeSaveData(self, file):
        output = open(file, 'wb')
        pickle.dump(self.saveData, output)
        output.close()
        
        print "Save Data written to file: " + file
    
    def loadSaveData(self, file):
        if not os.path.isfile(file):
            # Don't attempt to load files that don't exist!
            return
        input = open(file, 'rb')
        self.saveData = pickle.load(input)
        input.close()
        
        # Write the loaded data back to the items from __init__
        self.globalTagList = self.saveData[0]
        self.userOutfits = self.saveData[1]
        self.clothing = self.saveData[2]
        
        print "Save Data loaded from file: " + file
