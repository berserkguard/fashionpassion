from kivy.app import App
from AppData import AppData

# Import screens
from HomeScreen import *
from WardrobeScreen import *
from CalendarScreen import *
from PlanOutfitScreen import *
from AddItemScreen import *
from EditTagScreen import *
from ModifyTagScreen import *
from SelectItemScreen import *

# Create the AppData
appData = AppData()

class FashionPassion(App):

    def build(self):
        sm = appData.sm;
    
        appData.loadSaveData("test.dat");
    
        # Add the screens
        sm.add_widget(HomeScreen(appData))
        sm.add_widget(WardrobeScreen(appData))
        sm.add_widget(CalendarScreen(appData))
        sm.add_widget(PlanOutfitScreen(appData))
        sm.add_widget(AddItemScreen(appData))
        sm.add_widget(EditTagScreen(appData))
        sm.add_widget(ModifyTagScreen(appData))
        sm.add_widget(SelectItemScreen(appData))
        
        sm.current = 'HomeScreen'
        return sm

    def on_pause(self):
        print "on_pause"
        appData.writeSaveData("test.dat");
        super(FashionPassion, self).on_pause();
    
    def on_stop(self):
        print "on_stop"
        appData.writeSaveData("test.dat");
        super(FashionPassion, self).on_stop();
        
if __name__ == '__main__':
    FashionPassion().run()
