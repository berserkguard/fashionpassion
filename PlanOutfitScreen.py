from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
from IconButton import IconButton
from CalendarItem import CalendarItem
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.graphics import *
from Util import *
from Clothing import Clothing
import time
import datetime
import random
from AppData import AppData
from Outfits import Outfits

class OutfitView(Widget):
    
    def __init__(self, appData, **kwargs):
        super(OutfitView, self).__init__(**kwargs)

        self.sm = appData.sm;
        self.appData = appData;
        self.idx = appData.planOutfitIdx;
        
        self.bind(pos=self.redraw, size=self.redraw)

    def redraw(self, wid, args):
        outfits = Outfits(self);
        self.canvas.clear();
        self.canvas.add(outfits.mannequin);
        
        print str(self.idx);
        for clothing in self.appData.userOutfits[self.idx]:
            self.canvas.add(outfits.getFull(clothing.name));
            print clothing.name

class PlanOutfitScreen(Screen):

    def __init__(self, appData, **kwargs):
        
        super(PlanOutfitScreen, self).__init__(**kwargs)
        self.name = "PlanOutfitScreen"
        self.sm = appData.sm
        self.appData = appData
        layout = BoxLayout(padding=0, spacing=0);
        layout.orientation = 'vertical'
        
        layout.add_widget(self.createTop());
        layout.add_widget(self.createMiddleTop());
        layout.add_widget(self.createMiddleBot());
        layout.add_widget(self.createBottom());
        
        self.add_widget(layout);

    def on_pre_enter(self):
        if self.lbl:
            self.lbl.text = self.appData.planOutfitDay;
        
        if self.outfitView:
            self.outfitView.idx = self.appData.planOutfitIdx;
            #print str(self.outfitView.idx);
            self.outfitView.redraw(None, None);

    def createTop(self):
        return TitleBar("Plan Outfit", self.appData, "HomeScreen");

    def createMiddleTop(self):

        middle = BoxLayout(orientation='horizontal', padding=20, width=250, size_hint=(1, 0.3))
        self.outfitView = OutfitView(self.appData, size_hint=(.7, 1))
        middle.add_widget(self.outfitView)

        def vertBox(Widget):
            vbox = BoxLayout(orientation='vertical')
            
            vbox.add_widget(IconButton(on_press=self.button_pushed, source='assets//accessories button.png'))
            vbox.add_widget(IconButton(on_press=self.button_pushed, source='assets//topsbutton.png'))
            vbox.add_widget(IconButton(on_press=self.button_pushed, source='assets//bottoms button.png'))
            vbox.add_widget(IconButton(on_press=self.button_pushed, source='assets//footwear button.png'))
            
            return vbox
        
        right = vertBox(self)
        middle.add_widget(right)
        
        return middle

    def createMiddleBot(self):
        
        middle2 = BoxLayout(orientation='horizontal', padding=5, height=200, width=250, size_hint=(1, 0.02))
        startTime = time.time();
        day = datetime.date.fromtimestamp(startTime).strftime("%A")
        self.lbl = Label(text=day, font_size='30sp', size_hint=(.45, .1))

        middle2.add_widget(self.lbl)
        return middle2
        
    def createBottom(self):
        bottom = GridLayout(rows=1, padding=5, spacing=0, size_hint=(1, None))
        wardrobeButton = IconButton(on_press=self.button_pushed, source='assets//hanger.png')
        calendarButton = IconButton(on_press=self.button_pushed, source='assets//calendar.png')

        bottom.add_widget(wardrobeButton)
        bottom.add_widget(calendarButton)
                        
        return bottom

    def button_pushed(self, obj):
        if obj.source == "assets//backward.png":
            self.sm.transition.direction = 'right'
            self.sm.current = "HomeScreen"
        elif obj.source == "assets//hanger.png":
            self.sm.transition.direction = 'left'
            self.sm.current = "WardrobeScreen"
        elif obj.source == "assets//calendar.png":
            self.sm.transition.direction = 'left'
            self.sm.current = "CalendarScreen"
        
        elif obj.source == "assets//accessories button.png":
            self.appData.clothingType = 'accessories'
            self.sm.transition.direction = 'left'
            self.sm.current = "SelectItemScreen"
        elif obj.source == "assets//topsbutton.png":
            self.appData.clothingType = 'tops'
            self.sm.transition.direction = 'left'
            self.sm.current = "SelectItemScreen"
        elif obj.source == "assets//bottoms button.png":
            self.appData.clothingType = 'bottoms'
            self.sm.transition.direction = 'left'
            self.sm.current = "SelectItemScreen"
        elif obj.source == "assets//footwear button.png":
            self.appData.clothingType = 'feet'
            self.sm.transition.direction = 'left'
            self.sm.current = "SelectItemScreen"
            #self.on_pre_enter();
        
        print "button pushed: " + obj.source
