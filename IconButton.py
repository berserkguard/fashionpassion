from kivy.uix.image import Image
from kivy.event import EventDispatcher
from kivy.uix.behaviors import ButtonBehavior
from kivy.graphics import *

class IconButton(ButtonBehavior, Image):
    
    def __init__(self, **kwargs):
        
        super(IconButton, self).__init__(**kwargs)
        self.checked = False
