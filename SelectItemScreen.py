from Util import TitleBar
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from IconButton import IconButton
from sets import Set
from kivy.graphics import *
from kivy.uix.popup import Popup
from kivy.uix.label import Label
from Clothing import Clothing
import os

class SelectItemScreen(Screen):
    def __init__(self, appData, **kwargs):
        super(SelectItemScreen, self).__init__(**kwargs)

        self.name = "SelectItemScreen"
        self.appData = appData
        self.checkedClothing = []

        layout = self.layout = FloatLayout()
        layout.add_widget(self.createTop())
        layout.add_widget(self.createMiddle())
        self.add_widget(layout)

    def on_pre_enter(self):
        if not self.middle:
            return;
        
        self.middle.clear_widgets();
        middle = self.middle
        
        # LOL BULLSHITTING FTW
        self.tops = ['assets//clothing_icons//shirt.png', 'assets//clothing_icons//tanktop.png'];
        self.bottoms = ['assets//clothing_icons//jeans.png', 'assets//clothing_icons//skirt.png', 'assets//clothing_icons//shorts.png'];
        self.accessories = []
        self.feet = ['assets//clothing_icons//boots.png', 'assets//clothing_icons//sandals.png'];
        
        for i, clothing in enumerate(self.appData.clothing):
            #path = "assets//clothing_icons//" + clothing + ".png"
            cloth = clothing[0][0]
            btn = IconButton(width=243, size_hint =(None, 1), source=cloth)
            btn.bind(on_press=self.selectItem)
            
            print self.appData.clothingType
            
            if self.appData.clothingType == "tops":
                if cloth in self.tops:
                    middle.add_widget(btn)
            elif self.appData.clothingType == "bottoms":
                if cloth in self.bottoms:
                    middle.add_widget(btn)
            elif self.appData.clothingType == "accessories":
                if cloth in self.accessories:
                    middle.add_widget(btn)
            elif self.appData.clothingType == "feet":
                if cloth in self.feet:
                    middle.add_widget(btn)
        
    def createTop(self):
        return TitleBar("Select Item", self.appData, "PlanOutfitScreen")

    def createMiddle(self):
        self.middle = middle = GridLayout(rows=3, padding=10, spacing=20, size_hint=(None,1))
        middle.bind(minimum_width=middle.setter('width'))
        
        root = ScrollView(size_hint=(1, .6), pos_hint={'center_x': .5, 'center_y': .6}, do_scroll_x=True)
        root.add_widget(middle)
        return root

    def selectItem(self, obj):
        small = obj.source[obj.source.rfind('//')+2:-4]
        
        # Remove duplicate clothing types
        toRemove = []
        for clothing in self.appData.userOutfits[self.appData.planOutfitIdx]:
            name = "assets//clothing_icons//" + clothing.name + ".png"
            if self.appData.clothingType == "tops":
                if name in self.tops:
                    toRemove.append(clothing)
            elif self.appData.clothingType == "bottoms":
                if name in self.bottoms:
                    toRemove.append(clothing)
            elif self.appData.clothingType == "accessories":
                if name in self.accessories:
                    toRemove.append(clothing)
            elif self.appData.clothingType == "feet":
                if name in self.feet:
                    toRemove.append(clothing)

        for item in toRemove:
            self.appData.userOutfits[self.appData.planOutfitIdx].remove(item);
        
        self.appData.userOutfits[self.appData.planOutfitIdx].add(Clothing(small));
        self.appData.sm.transition.direction = 'right'
        self.appData.sm.current = 'PlanOutfitScreen'
