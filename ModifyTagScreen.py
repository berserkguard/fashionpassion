from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.dropdown import DropDown
from kivy.uix.togglebutton import ToggleButton
from kivy.event import EventDispatcher
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.listview import ListView
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from IconButton import IconButton
from Util import *
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
from sets import Set
import copy

class TagPopup(Popup):
    def __init__(self, appData, grid, **kwargs):
        self.appData = appData
        self.grid = grid
    
        root = BoxLayout(orientation='vertical', spacing=0, padding=0, height=80, size_hint=(1,1))
        upper = BoxLayout(orientation='horizontal', spacing=0, padding=0, size_hint=(1,None))
        lower = BoxLayout(orientation='horizontal', spacing=0, padding=0, size_hint=(1,None))
        
        label = Label(text='Tag Name:', size_hint_x=0.4);
        upper.add_widget(label)

        self.textinput = textinput = TextInput(text='Hello world', multiline=False, size_hint_x=0.6)
        upper.add_widget(textinput)
        
        add = Button(text='Add', size_hint=(.4,.4))
        lower.add_widget(add)
        
        cancel = Button(text='Cancel', size_hint=(.4,.4))
        lower.add_widget(cancel)

        root.add_widget(upper)
        root.add_widget(lower)

        cancel.bind(on_press=self.dismiss)
        add.bind(on_press=self.onAdd)
        
        super(TagPopup, self).__init__(title='Add Tag', content=root, size_hint=(.8, .5))

    def onAdd(self, instance):
        self.dismiss()
        self.appData.globalTagList.add(self.textinput.text)
        for i,elem in enumerate(self.appData.clothing):
            for j,elem2 in enumerate(self.appData.modifyClothing):
                if elem[0][0] == elem2:
                    self.appData.clothing[i][0][1].add(self.textinput.text)
        self.appData.modifyClothing.clear()
        self.appData.modifyClothingTags.clear()
        self.appData.sm.transition.direction = 'left'
        self.appData.sm.current = "WardrobeScreen"

class ModifyTagScreen(Screen):
    
    def __init__(self, appData, **kwargs):
        super(ModifyTagScreen, self).__init__(**kwargs)
        self.name = "ModifyTagScreen"
        self.sm = appData.sm
        self.appData = appData
        self.removeList = []
        self.layout = layout = FloatLayout()
        
        layout.add_widget(self.createTop());
        layout.add_widget(self.createMiddle());
        layout.add_widget(self.createBottom());
        
        self.add_widget(layout);
        
    def createTop(self):
        return TitleBar("Modify Tags", self.appData, "WardrobeScreen")
    
    def createMiddle(self):
        self.grid = grid = GridLayout(rows=10, padding=10, spacing=0, size_hint=(.5,.5), pos_hint={'center_x': .5, 'center_y': .5});
        grid.bind(minimum_width=grid.setter('width'))
        for tag in self.appData.modifyClothingTags:
            btn = ToggleButton(text=tag)
            btn.bind(state=self.addToRemoveList)
            grid.add_widget(btn)
        return grid

    def addToRemoveList(self, obj, value):
        if value == 'down':
            self.removeList.append(obj.text)
        else:
            self.removeList.remove(obj.text)
    
    def createBottom(self):
        bot = FloatLayout()

        add = IconButton(on_press=self.button_pushed, pos_hint={'x':.33, 'y':.05}, size_hint=(.1,.1), source='assets//add.png')
        bot.add_widget(add)

        delete = IconButton(on_press=self.button_pushed, pos_hint={'x':.66, 'y':.05}, size_hint=(.1,.1), source='assets//remove.png')
        bot.add_widget(delete)
        
        return bot

    def on_enter(self):
        if not self.grid:
            return

        self.grid.clear_widgets()
        self.layout.add_widget(self.createMiddle())

        if not self.removeList:
            return

        self.removeList = []
    
    def addTag(self):
        popup = TagPopup(self.appData, self.grid)
        popup.open();
        
    def removeTag(self):
        if len(self.removeList) == 0:
            grid = GridLayout(rows=2, padding=10, spacing=20)
            grid.add_widget(Label(text='Please select items to remove'))
            but = Button(text='Close')
            grid.add_widget(but)
            popup = Popup(title='Error', content=grid, size_hint=(.4,.3))
            but.bind(on_press=popup.dismiss)
            popup.open()
        else:
            copyList = copy.deepcopy(self.appData.clothing)
            for i,elem in enumerate(self.removeList):
                for j,elem2 in enumerate(self.appData.clothing):
                    for k,elem3 in enumerate(elem2[0][1]):
                        if elem == elem3:
                            copyList[j][0][1].remove(elem)
            self.appData.clothing = copy.deepcopy(copyList)
            self.appData.modifyClothing.clear()
            self.appData.modifyClothingTags.clear()
            self.appData.sm.transition.direction = 'left'
            self.appData.sm.current = "WardrobeScreen"

    def button_pushed(self, obj):
        if obj.source == "assets//add.png":
            self.addTag()
        elif obj.source == "assets//remove.png":
            self.removeTag()
