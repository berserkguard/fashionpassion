from Util import TitleBar
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.button import Button
from kivy.uix.scrollview import ScrollView
from IconButton import IconButton
from sets import Set
from kivy.graphics import *
from kivy.uix.popup import Popup
from kivy.uix.label import Label
import os

class AddItemScreen(Screen):
    def __init__(self, appData, **kwargs):
        super(AddItemScreen, self).__init__(**kwargs)

        self.name = "AddItemScreen"
        self.appData = appData
        self.checkedClothing = []
        self.clothingTuple = [("assets//clothing_icons//boots.png",Set(['footwear'])),
                              ("assets//clothing_icons//jeans.png",Set(['bottoms'])),
                              ("assets//clothing_icons//sandals.png",Set(['footwear'])),
                              ("assets//clothing_icons//shirt.png",Set(['tops'])),
                              ("assets//clothing_icons//shorts.png",Set(['bottoms'])),
                              ("assets//clothing_icons//skirt.png",Set(['bottoms'])),
                              ("assets//clothing_icons//tanktop.png",Set(['tops']))]
        layout = self.layout = FloatLayout()
        layout.add_widget(self.createTop())
        layout.add_widget(self.createMiddle())
        layout.add_widget(self.createBot())
        self.add_widget(layout)

    def createTop(self):
        return TitleBar("Add Item", self.appData, "WardrobeScreen")

    def createMiddle(self):
        middle = GridLayout(rows=3, padding=10, spacing=20, size_hint=(None,1))
        middle.bind(minimum_width=middle.setter('width'))
        icon_list = os.listdir("assets//clothing_icons//")
        for i, elem in enumerate(icon_list):
            path = "assets//clothing_icons//" + elem
            btn = IconButton(text='cloth %d' % i, width=243, size_hint=(None, 1), source=path)
            btn.bind(on_press=self.updateGraphics)
            middle.add_widget(btn)
        root = ScrollView(size_hint=(1, .6), pos_hint={'center_x': .5, 'center_y': .6}, do_scroll_x=True)
        root.add_widget(middle)
        return root

    def createBot(self):
        bot = GridLayout(cols=2, padding=10, spacing=100, size_hint=(.5,.1), pos_hint={'center_x': .5, 'center_y':.1})
        add = Button(text='add', size_hint=(.1,.1))
        add.bind(on_press=self.addItem)
        bot.add_widget(add)

        delete = Button(text='cancel', size_hint=(.1,.1))
        bot.add_widget(delete)
        return bot

    def addItem(self, obj):
        if len(self.checkedClothing) == 0:
            grid = GridLayout(rows=2, padding=10, spacing=20)
            grid.add_widget(Label(text='Please select items to add'))
            but = Button(text='Close')
            grid.add_widget(but)
            popup = Popup(title='Error', content=grid, size_hint=(.4,.3))
            but.bind(on_press=popup.dismiss)
            popup.open()
        else:
            grid = GridLayout(rows=3, padding=10, spacing=20)
            grid.add_widget(Label(text='Are you sure?'))
            but = Button(text='No')
            but2 = Button(text='Yes')
            grid.add_widget(but2)
            grid.add_widget(but)
            popup = self.popup = Popup(title='Add Items', content=grid, size_hint=(.4,.35))
            but.bind(on_press=popup.dismiss)
            but2.bind(on_press=self.addToAppData)
            popup.open()

    def addToAppData(self, obj):
        for i, elem in enumerate(self.checkedClothing):
            item_tuple = [item for item in self.clothingTuple if item[0] == elem]
            self.appData.clothing.append(item_tuple)
        self.popup.dismiss()
        self.appData.sm.transition.direction = "left"
        self.appData.sm.current = "WardrobeScreen"
        

    def updateGraphics(self, obj):
        obj.canvas.clear()
        if obj.checked == False:
            with obj.canvas:
                Color(.5, .5, .5)
                Rectangle(pos=obj.pos, size=obj.size)
                Color(1, 1, 1)
                cx = obj.center_x - obj.texture_size[0] / 2.
                cy = obj.center_y - obj.texture_size[1] / 2.
                Rectangle(texture=obj.texture, pos=(cx, cy), size=obj.texture_size)
            self.checkedClothing.append(obj.source)
            obj.checked = True
        else:
            with obj.canvas:
                Color(0, 0, 0)
                Rectangle(pos=obj.pos, size=obj.size)
                Color(1, 1, 1)
                cx = obj.center_x - obj.texture_size[0] / 2.
                cy = obj.center_y - obj.texture_size[1] / 2.
                Rectangle(texture=obj.texture, pos=(cx, cy), size=obj.texture_size)
            self.checkedClothing.remove(obj.source)
            obj.checked = False
