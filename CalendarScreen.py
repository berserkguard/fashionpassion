from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
from IconButton import IconButton
from CalendarItem import CalendarItem
from kivy.core.window import Window
from kivy.graphics import *
import time
import datetime
import random
from Util import TitleBar

class CalendarScreen(Screen):

    def __init__(self, appData, **kwargs):
        super(CalendarScreen, self).__init__(**kwargs)
        self.name = "CalendarScreen"
        self.appData = appData
        
        layout = BoxLayout(padding=0, spacing=0);
        layout.orientation = 'vertical'
        
        layout.add_widget(self.createTop());
        layout.add_widget(self.createMiddle());
        layout.add_widget(self.createBottom());
        
        self.add_widget(layout);

    def on_pre_enter(self):
        if self.middle:
            for child in self.middle.children:
                child.refresh();
        
    def createTop(self):
        return TitleBar("Calendar", self.appData, "HomeScreen");
        #top = GridLayout(rows=1, padding=5, spacing=0, size_hint=(None, 0.1))
        #top.add_widget(IconButton(on_press=self.button_pushed, source='assets//backward.png'))
        #return top
        
    def createMiddle(self):
        self.middle = middle = GridLayout(rows=1, padding=10, spacing=20, size_hint=(None, 1))

        # when we add children to the grid layout, its size doesn't change at
        # all. we need to ensure that the width will be the minimum required to
        # contain all the childs. (otherwise, we'll child outside the bounding
        # box of the childs)
        middle.bind(minimum_width=middle.setter('width'))

        # add calendar items into the grid
        startTime = time.time();
        weatherTypes = ["sunny", "cloudy", "rainy"];
        for i in range(7):
            day = datetime.date.fromtimestamp(startTime + i * 24*60*60).strftime("%A")
            lo = random.randint(20, 50)
            hi = random.randint(50, 80)
            weather = random.randint(0, 2)
            item = CalendarItem(self.appData, day, i, lo, hi, weatherTypes[weather])
            middle.add_widget(item)
        
        # create a scroll view, with a size < size of the grid
        root = ScrollView(size_hint=(1, 0.6), pos_hint={'center_x': .5, 'center_y': .5}, do_scroll_x=True)
        root.add_widget(middle)
        
        return root
    
    def createBottom(self):
        bottom = GridLayout(rows=1, padding=5, spacing=0, size_hint=(1, 0.3))
        bottom.add_widget(IconButton(on_press=self.button_pushed, source='assets//hanger.png'))
        return bottom

    def button_pushed(self, obj):
        if obj.source == "assets//backward.png":
            self.appData.sm.transition.direction = 'right'
            self.appData.sm.current = "HomeScreen"
        elif obj.source == "assets//hanger.png":
            self.appData.sm.transition.direction = 'left'
            self.appData.sm.current = "WardrobeScreen"
            
        print "button pushed: " + obj.source
