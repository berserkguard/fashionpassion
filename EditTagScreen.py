from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.dropdown import DropDown
from kivy.uix.togglebutton import ToggleButton
from kivy.event import EventDispatcher
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.listview import ListView
from kivy.uix.popup import Popup
from kivy.uix.widget import Widget
from IconButton import IconButton
from Util import *
from kivy.uix.textinput import TextInput

class TagPopup(Popup):
    def __init__(self, appData, grid, **kwargs):
        self.appData = appData
        self.grid = grid
    
        root = BoxLayout(orientation='vertical', spacing=0, padding=0, height=80, size_hint=(1,1))
        upper = BoxLayout(orientation='horizontal', spacing=0, padding=0, size_hint=(1,None))
        lower = BoxLayout(orientation='horizontal', spacing=0, padding=0, size_hint=(1,None))
        
        label = Label(text='Tag Name:', size_hint_x=0.4);
        upper.add_widget(label);

        self.textinput = textinput = TextInput(text='Hello world', multiline=False, size_hint_x=0.6)
        upper.add_widget(textinput);
        
        add = Button(text='Add', size_hint=(.4,.4))
        lower.add_widget(add)
        
        cancel = Button(text='Cancel', size_hint=(.4,.4))
        lower.add_widget(cancel)

        root.add_widget(upper)
        root.add_widget(lower)

        cancel.bind(on_press=self.dismiss)
        add.bind(on_press=self.onAdd)
        
        super(TagPopup, self).__init__(title='Add Tag', content=root, size_hint=(.8, .5))

    def onAdd(self, instance):
        self.dismiss();
        self.appData.globalTagList.add(self.textinput.text);
        
        self.grid.clear_widgets();
        for tag in self.appData.globalTagList:
            btn = ToggleButton(text=tag, size_hint_y=None, height=40);
            self.grid.add_widget(btn);
        
        print self.textinput.text

class EditTagScreen(Screen):
    
    def __init__(self, appData, **kwargs):
        super(EditTagScreen, self).__init__(**kwargs)
        self.name = "EditTagScreen"
        self.sm = appData.sm
        self.appData = appData
        layout = BoxLayout(orientation='vertical')
        
        layout.add_widget(self.createTop());
        layout.add_widget(self.createMiddle());
        layout.add_widget(self.createBottom());
        
        self.add_widget(layout);
        
    def createTop(self):
        return TitleBar("Edit Tags", self.appData, "WardrobeScreen")
    
    def createMiddle(self):
        self.grid = grid = BoxLayout(orientation='vertical', size_hint=(1, 0.7), padding=40);
        for tag in self.appData.globalTagList:
            btn = ToggleButton(text=tag, size_hint_y=None, height=40);
            grid.add_widget(btn);
        return grid
    
    def createBottom(self):
        bottom = BoxLayout(orientation='horizontal', size_hint=(1, 0.2))
                
        addButton = IconButton(on_press=self.button_pushed, source='assets//add.png')
        removeButton = IconButton(on_press=self.button_pushed, source='assets//remove.png')       
        #editButton = IconButton(on_press=self.button_pushed, source='assets//edit button.png')     
                
        bottom.add_widget(addButton)
        bottom.add_widget(removeButton)
        #bottom.add_widget(editButton)
        
        return bottom
    
    def addTag(self):
        popup = TagPopup(self.appData, self.grid);
        popup.open();
        
    def removeTag(self):
        def onYes():
            toRemove = []
            for child in self.grid.children:
                if child.state == 'down':
                    toRemove.append(child);
            for child in toRemove:
                self.appData.globalTagList.remove(child.text);
                self.grid.remove_widget(child);
        popup = ConfirmationPopup(onYes);
        popup.open();
        
    def editTag(self):
        #not implemented. Too complex for this build
        return
            
    
    def button_pushed(self, obj):
        if obj.source == "assets//add.png":
            self.addTag()
        elif obj.source == "assets//remove.png":
            self.removeTag()
        elif obj.source == "assets//edit button.png":
            self.editTag()
            
            
    